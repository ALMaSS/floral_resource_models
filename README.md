The floral resource models are being developed within the **B-GOOD project** (https://b-good-project.eu/). The methodology can be find in the report (B-GOOD Deliverable D3.3) in the _Documents_ folder. The _Documents_ folder also includes 3 Appendices:
- Appendix A: Database summarizing floral phenology models of important bee-friendly species [_Floral_phenology_models.xlsx_];
- Appendix B: Database summarizing production of floral resources by important bee-friendly species [_Resources_production_flower_per_day.xlsx_]; and
- Appendix C: Database describing composition of bee-friendly species in different EUNIS habitat types together with habitat-specific flower density [_Habitat_specific_flowering_density.xlsx_].

The floral resource models can be considered ‘living’ models and are designed in such a way that they can be easily updated when more/additional data on pollen, nectar or sugar production, phenology of bee-friendly species or plant species composition in landscape elements / habitat units important for bees, are made available. It is the intention that any new data be added to the database during and also after the project finishes.

Floral resource models can be generated using an interactive script in Python for Jupyter Notebook: **_Resources_calculator.ipynb_**. This script allows to calculate floral resources available for bees in different habitat units in different years and locations, and generate outputs for the ALMaSS pollinators models.

**Run _Resources calculator_ interactively on Binder**

Access using the following URL:
https://mybinder.org/v2/gl/ALMaSS%2Ffloral_resource_models/HEAD?filepath=Resources_calculator.ipynb
