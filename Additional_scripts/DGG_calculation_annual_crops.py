import os
import numpy as np
import pandas as pd
import netCDF4 as nc
import calendar
#from scipy import stats    #for shapiro-wilk in the future
pd.set_option('display.max_columns', None)




species_nr = 241
Tbase = 4
how_much = 3000 # how big sample to test
# Input paths
base_dir = "E:/Flowering/"
plant_data = os.path.join(base_dir)
E_OBS_data = os.path.join(base_dir, "input_data/E_OBS_data/")

# Output file
output = os.path.join(base_dir, f"results/{species_nr}stat.csv")
output2 = os.path.join(base_dir, f"results/{species_nr}.csv")
output3 = os.path.join(base_dir, f"results/{species_nr}fullresult.csv")
# ### E-OBS data importing and pre-processing
# Importing elevation data
elev = nc.Dataset(E_OBS_data + "elev_ens_0.1deg_reg_v22.0e.nc")

# Extracting lon, lat data
lats = elev.variables['latitude'][:]
lons = elev.variables['longitude'][:]

# E_OBS data regions
reg1 = list(range(1950, 1965))
reg2 = list(range(1965, 1980))
reg3 = list(range(1980, 1995))
reg4 = list(range(1995, 2011))
reg5 = list(range(2011, 2021))


# this function returs region no and start year
def eobs_data_reg(year):
    if (year in reg1):
        return [1950, 1964]
    elif (year in reg2):
        return [1965, 1979]
    elif (year in reg3):
        return [1980, 1994]
    elif (year in reg4):
        return [1995, 2010]
    else:
        return [2011, 2020]


# Find the nearest latitude and longitude for a given location (indexes)
def getclosest_idx(latpt, lonpt):
    lat_idx = np.abs(lats - latpt).argmin()
    lon_idx = np.abs(lons - lonpt).argmin()
    return [lat_idx, lon_idx]


# Importing species phenological data

species_data_all = pd.read_csv(plant_data + "Database_crop_phenology.csv", sep=";")

### Processing data

#limitting  dataset to years available in 65 and 69
sp60 = species_data_all[(species_data_all["PLANT_NO"] == int(species_nr)) & (species_data_all["BBCH"] == 60)]
if len(sp60["YEAR"]) > 3000:
    sp6569 = species_data_all[(species_data_all["PLANT_NO"] == int(species_nr)) & (species_data_all["BBCH"] != 60 )]
    if sp6569.empty():
        ylow = 0
        yhigh = 3000
    else:
        yearstocount= sp6569["YEAR"]
        ylow = int(yearstocount.min())
        yhigh = int(yearstocount.max())

    print (ylow, yhigh)
else:
    ylow = 0
    yhigh =3000

species_data = species_data_all[(species_data_all["PLANT_NO"] == int(species_nr)) & (species_data_all["BBCH"] == 60) & (species_data_all["YEAR"] >= ylow) & (species_data_all["YEAR"]<= yhigh) ].head(how_much)
print (species_data)

count = 0
for y in range(0, 11):
    species_base_temp = y
    species_data[y] = np.nan
result = pd.DataFrame(columns=species_data.columns)
result["best"] = np.nan
for index, row in species_data.iterrows():
    count = count + 1
    print("Processed " + str(count) + " out of " + str(len(species_data)))



    # Getting the slice of the E-OBS data for a given (station) location and for a given year up to flowering start
    # Finding the closest location in E-OBS grid
    lat_idx = getclosest_idx(row['LAT'], row['LON'])[0]
    lon_idx = getclosest_idx(row['LAT'], row['LON'])[1]

    if row['SYEAR'] == row['YEAR']:
        # We have non-crop plant OR analyzed crop developmental stage is in the same year as sowing
        print("We have non-crop plant OR analyzed crop developmental stage is in the same year as sowing")
        # accesing the proper E-OBS files
        start = str(eobs_data_reg(row['YEAR'])[0])
        end = str(eobs_data_reg(row['YEAR'])[1])
        min_temp = nc.Dataset(E_OBS_data + "tn_ens_mean_" + start + "-" + end + ".nc").variables['tn']
        max_temp = nc.Dataset(E_OBS_data + "tx_ens_mean_" + start + "-" + end + ".nc").variables['tx']

        # Finding start index for days to be sliced
        total_days = 0
        years = list(range(eobs_data_reg(row['YEAR'])[0], int(row['YEAR'])))
        if len(years) > 0:
            for year in years:
                if calendar.isleap(year):
                    total_days = total_days + 366
                else:
                    total_days = total_days + 365

        # Getting slices for min and max daily temp
        slice_min_temp = min_temp[int(total_days + row['SDAY']):int(total_days + row['DAY']), lat_idx, lon_idx]
        slice_max_temp = max_temp[int(total_days + row['SDAY']):int(total_days + row['DAY']), lat_idx, lon_idx]

        # Checking if the no of days is ok
        if ((row['DAY'] - row['SDAY']) != slice_max_temp.count()):
            print("No of days to count GDD is not equal to DOY of analyzed phase")

        # Calculating length of phenological phase from January 1st or sowing date
        species_data.loc[index, 'L'] = row['DAY'] - row['SDAY']
        print('Length of phase: ' + str(row['DAY'] - row['SDAY']))

    else:
        # We have winter crop and analyzing developmental stage after spring renewal of vegetation
        # (so we are crossing the end of year)
        print("We have winter crop and analyzing developmental stage after spring renewal of vegetation")
        start = str(eobs_data_reg(row['YEAR'])[0])
        end = str(eobs_data_reg(row['YEAR'])[1])
        sstart = str(eobs_data_reg(row['SYEAR'])[0])
        send = str(eobs_data_reg(row['SYEAR'])[1])

        # Finding start index for days to be sliced
        total_days = 0
        years = list(range(eobs_data_reg(row['SYEAR'])[0], int(row['SYEAR'])))
        if len(years) > 0:
            for year in years:
                if calendar.isleap(year):
                    total_days = total_days + 366
                else:
                    total_days = total_days + 365
        if calendar.isleap(row['SYEAR']):
            total_days_end = total_days + 366
        else:
            total_days_end = total_days + 365

        if (start == sstart) & (end == send):
            # only one E-OBS data file to import
            print("One E-OBS data file to import")
            min_temp = nc.Dataset(E_OBS_data + "tn_ens_mean_" + start + "-" + end + ".nc").variables['tn']
            max_temp = nc.Dataset(E_OBS_data + "tx_ens_mean_" + start + "-" + end + ".nc").variables['tx']

            # Getting slices for min and max daily temp


            slice_min_temp1 = min_temp[int(total_days + row['SDAY']):int(total_days_end - 1), lat_idx, lon_idx]
            slice_min_temp2 = min_temp[int(total_days_end):int(total_days_end + row['DAY']), lat_idx, lon_idx]
            slice_min_temp = np.concatenate((slice_min_temp1, slice_min_temp2))
            slice_max_temp1 = max_temp[int(total_days + row['SDAY']):int(total_days_end - 1), lat_idx, lon_idx]
            slice_max_temp2 = max_temp[int(total_days_end):int(total_days_end + row['DAY']), lat_idx, lon_idx]
            slice_max_temp = np.concatenate((slice_max_temp1, slice_max_temp2))

        else:
            # two E-OBS data files to import
            print("Two E-OBS data file to import")
            smin_temp = nc.Dataset(E_OBS_data + "tn_ens_mean_" + sstart + "-" + send + ".nc").variables['tn']
            smax_temp = nc.Dataset(E_OBS_data + "tx_ens_mean_" + sstart + "-" + send + ".nc").variables['tx']
            min_temp = nc.Dataset(E_OBS_data + "tn_ens_mean_" + start + "-" + end + ".nc").variables['tn']
            max_temp = nc.Dataset(E_OBS_data + "tx_ens_mean_" + start + "-" + end + ".nc").variables['tx']

            # Getting slices for min and max daily temp
            slice_min_temp1 = smin_temp[int(total_days + row['SDAY']):int(total_days_end - 1), lat_idx, lon_idx]
            slice_min_temp2 = min_temp[0:int(row['DAY']), lat_idx, lon_idx]
            slice_min_temp = np.concatenate((slice_min_temp1, slice_min_temp2))
            slice_max_temp1 = smax_temp[int(total_days + row['SDAY']):int(total_days_end - 1), lat_idx, lon_idx]
            slice_max_temp2 = max_temp[0:int(row['DAY']), lat_idx, lon_idx]
            slice_max_temp = np.concatenate((slice_max_temp1, slice_max_temp2))

        # Checking if the no of days is ok
        if (row['DAY'] != slice_max_temp2.count()):
            print("No of days to count GDD is not equal to DOY of analyzed phase")

        # Calculating length of phenological phase from January 1st or sowing date
        species_data.loc[index, 'L'] = total_days_end - 1 - total_days - row['SDAY'] + row['DAY']
        print('Length of phase: ' + str(total_days_end - 1 - total_days - row['SDAY'] + row['DAY']))



    # Calculating daily GDD and saving to a list
    # If daily min or max temp < base temp then daily GDD is set to 0
    # Otherwise average of the daily maximum and minimum temperatures minus base temp is used
    for y in range(0, 11):  # ittereate temperature from 0 to 10
        species_base_temp = y

        daily_GDD = []

        for i in range(0, slice_max_temp.count()):
            if ((slice_max_temp[i] + slice_min_temp[i]) / 2) < species_base_temp:
                daily_GDD.append(0)
            else:
                daily_GDD.append(((slice_max_temp[i] + slice_min_temp[i]) / 2) - species_base_temp)


        # Summing up daily GDD up to start of a given phenological phase
        dailyGDDno0 = [i for i in daily_GDD if i > 0]
        if len(dailyGDDno0) < 15:
            print("not enough days")
            GDD_sum = -1
        else:

            GDD_sum = sum(daily_GDD)

        print('Sum of GDD is: ' + str(GDD_sum))
        row[y] = GDD_sum

    result = result.append(row)

#shapiros=[]

Tbase_arbitrary = Tbase #this is an arbitrary choice of T base
##this is to test the best Tbase but it is not used now
# for i in range(7, 18):
#     tst = result.iloc[:, i]
#     tst1 = stats.shapiro(tst)
#     shapiros.append(tst1.pvalue)
# print ('list p values')
# print(shapiros)
# max_value = max(shapiros)
# max_index = shapiros.index(max_value)
# Tbase = Tbase_arbitrary #max_index
# locum = 9 + Tbase_arbitrary #max_index
val = -(11 - Tbase)
Tbest = result.iloc[:,val ]
result["best"] = result[Tbest]
print(result)
print("for plant ", species_nr, " Tbase is set to ", Tbase)

#other phases
col = ["plant", "60mean", "60std", "65mean", "65std", "69mean", "69std"]
resultstat = pd.DataFrame(columns=col)

col2 = ["plant", "60", "65", "69"]
resultsum = pd.DataFrame(columns=col2)


other_phases = [65,69]  #type as many phases as you have in dataset
for op in other_phases:

    species_data = species_data_all[(species_data_all["PLANT_NO"] == int(species_nr)) & (species_data_all["BBCH"] == int(op))]
    print(species_data)
    if species_data.empty:
        print('BBCH ',op,' not available')
    else:
        print('calculating BBCH', op)
        count = 0
        result60 = result

        for index, row in species_data.iterrows():
            res60 = result60[(result60['YEAR'] == row['YEAR']) & (round(result60['LAT']) == round(row['LAT'])) & (round(result60['LON']) == round(row['LON']))]            #print (res60, )

            print("res60", res60)

            ###
            GDD60no0 = res60[res60["best"] > 0]
            GDD60 = GDD60no0["best"].mean()
            print ("this is the value you want to know",GDD60)

            count = count + 1
            print("Processed " + str(count) + " out of " + str(len(species_data)))


            lat_idx = getclosest_idx(row['LAT'], row['LON'])[0]
            lon_idx = getclosest_idx(row['LAT'], row['LON'])[1]


            start = str(eobs_data_reg(row['YEAR'])[0])
            end = str(eobs_data_reg(row['YEAR'])[1])
            min_temp = nc.Dataset(E_OBS_data + "tn_ens_mean_" + start + "-" + end + ".nc").variables['tn']
            max_temp = nc.Dataset(E_OBS_data + "tx_ens_mean_" + start + "-" + end + ".nc").variables['tx']


            total_days = 0
            years = list(range(eobs_data_reg(row['YEAR'])[0], int(row['YEAR'])))

            if len(years) > 0:
                for year in years:
                    if calendar.isleap(year):
                        total_days = total_days + 366
                    else:
                        total_days = total_days + 365



            slice_min_temp = min_temp[(total_days):int(total_days + row['DAY']), lat_idx, lon_idx]
            slice_max_temp = max_temp[(total_days):int(total_days + row['DAY']), lat_idx, lon_idx]

            species_base_temp = Tbase

            daily_GDD = []

            for i in range(0, slice_max_temp.count()):
                if ((slice_max_temp[i] + slice_min_temp[i]) / 2) < species_base_temp:
                    daily_GDD.append(0)
                else:
                    daily_GDD.append(((slice_max_temp[i] + slice_min_temp[i]) / 2) - species_base_temp)


            # dailyGDDno0 = [i for i in daily_GDD if i >0]
            # if len(dailyGDDno0)<15:
            #     print ("not enough days")
            #     GDD_sum = -1
            # else:

            GDD_sum = sum(daily_GDD)
            GDDphase_interval = GDD_sum - GDD60

            print('Sum of phase GDD is: ' + str(GDDphase_interval))
            row["best"] = GDDphase_interval
            result = result.append(row)


print (result)
m60pp = result[result["BBCH"] == 60]
m60p = m60pp[m60pp["best"] > 0]
m60 = m60p["best"].mean()
s60 = m60p["best"].std()

m65pp = result[result["BBCH"] == 65]
m65p = m65pp[m65pp["best"] > 0]
m65 = m65p["best"].mean()
s65 = m65p["best"].std()

m69pp = result[result["BBCH"] == 69]
m69p = m69pp[m69pp["best"] > 0]
m69 = m69p["best"].mean()
s69 = m69p["best"].std()

print (m60, s60)
print (m65, s65)
print (m69, s69)

if m65p.empty:
    plantsumrow = [species_nr, m60, 0, (m60+m69)]

elif m69p.empty:
    if m65 > 50:
        plantsumrow = [species_nr, m60, (m60 + m65), 0]
    else:
        plantsumrow = [species_nr, m60, 0, 0]
        m65 = 0
        s65 = 0

elif(m65p.empty) & (m69p.empty):
    plantsumrow = [species_nr, m60, 0, 0]

else:
    if  m65 > (m69/5):
        plantsumrow = [species_nr, m60, (m60 + m65), (m60 + m69)]
    else:
        plantsumrow = [species_nr, m60, 0, (m60 + m69)]
        m65 = 0
        s65 = 0

plantstatrow = [species_nr, m60, s60, m65, s65, m69, s69]
print (plantstatrow)
resultstat.loc[0] = plantstatrow


print (plantsumrow)
resultsum.loc[0] = plantsumrow

print(resultsum)
print(resultstat)
resultstat.to_csv(output)
resultsum.to_csv(output2)
result.to_csv(output3)
